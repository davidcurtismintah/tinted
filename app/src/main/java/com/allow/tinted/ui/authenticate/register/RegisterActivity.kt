package com.allow.tinted.ui.authenticate.register

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.allow.tinted.R
import com.allow.tinted.ui.authenticate.login.LoginActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    companion object {
        fun startIntent(ctx: Context) = Intent(ctx, RegisterActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        login_in_button.setOnClickListener {
            startActivity(LoginActivity.startIntent(this))
            finish()
        }
    }
}
