package com.allow.tinted.ui.authenticate.socialauth.linkedInSiginIn;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.allow.tinted.ui.authenticate.socialauth.linkedInSiginIn.linkedInApi.LinkedinAuthManager;

/**
 * Created by multidots on 6/17/2016.<p>
 * Lined in login helper. This class will handle linked in access token and get user profile.
 *
 * @see 'https://www.numetriclabz.com/android-linkedin-integration-login-tutorial/'
 */
public class LinkedInHelper {
    private Activity mContext;
    private LinkedInResponse mListener;
    private static final String HOST = "api.linkedin.com";
    private static final String USER_DETAIL_URL = "https://" + HOST + "/v1/people/~:" +
            "(email-address,formatted-name,phone-numbers,public-profile-url,picture-url,picture-urls::(original))";

    /**
     * Public constructor.
     *
     * @param context  instance of the caller.
     * @param listener {@link LinkedInResponse} for callback.
     */
    public LinkedInHelper(@NonNull Activity context, @NonNull LinkedInResponse listener) {
        mContext = context;
        mListener = listener;
    }

    public void performSignIn() {
        new LinkedinAuthManager(mContext, mListener).showLinkedin();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

}
