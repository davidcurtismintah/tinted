package com.allow.tinted.ui.authenticate.socialauth.linkedInSiginIn;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by multidots on 6/17/2016.<p>
 * This class represents linked in user profile.
 */
public class LinkedInUser {

    @NonNull
    public String name;

    @NonNull
    public String email;

    @Nullable
    public String phone;

    @Nullable
    public String pictureUrl;
}
