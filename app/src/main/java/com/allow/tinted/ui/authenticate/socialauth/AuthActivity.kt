package com.allow.tinted.ui.authenticate.socialauth

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.allow.tinted.R
import com.allow.tinted.ui.authenticate.register.RegisterActivity
import com.allow.tinted.ui.authenticate.socialauth.facebookSignIn.FacebookHelper
import com.allow.tinted.ui.authenticate.socialauth.facebookSignIn.FacebookResponse
import com.allow.tinted.ui.authenticate.socialauth.facebookSignIn.FacebookUser
import com.allow.tinted.ui.authenticate.socialauth.googleAuthSignin.GoogleAuthResponse
import com.allow.tinted.ui.authenticate.socialauth.googleAuthSignin.GoogleAuthUser
import com.allow.tinted.ui.authenticate.socialauth.googleAuthSignin.GoogleSignInHelper
import com.allow.tinted.ui.authenticate.socialauth.linkedInSiginIn.LinkedInHelper
import com.allow.tinted.ui.authenticate.socialauth.linkedInSiginIn.LinkedInResponse
import com.allow.tinted.ui.authenticate.socialauth.linkedInSiginIn.LinkedInUser
import kotlinx.android.synthetic.main.activity_socialauth.*
import java.security.MessageDigest


class AuthActivity : AppCompatActivity(), View.OnClickListener,
    FacebookResponse,
    LinkedInResponse,
    GoogleAuthResponse {

    private lateinit var mFbHelper: FacebookHelper
    private lateinit var mGAuthHelper: GoogleSignInHelper
    private lateinit var mLinkedInHelper: LinkedInHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_socialauth)

        //google auth initialization
        mGAuthHelper = GoogleSignInHelper(this, null, this)

        //fb api initialization
        mFbHelper = FacebookHelper(
            this,
            "id,name,email,gender,birthday,picture,cover",
            this
        )

        //linkedIn initializer
        mLinkedInHelper = LinkedInHelper(this, this)

        //set sign in button
        g_login_btn.setOnClickListener(this)
        bt_act_login_fb.setOnClickListener(this)
        linkedin_login_button.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.g_login_btn -> mGAuthHelper.performSignIn(this)
            R.id.bt_act_login_fb -> mFbHelper.performSignIn(this)
            R.id.linkedin_login_button -> mLinkedInHelper.performSignIn()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //handle results
        mFbHelper.onActivityResult(requestCode, resultCode, data)
        mGAuthHelper.onActivityResult(requestCode, resultCode, data)
        mLinkedInHelper.onActivityResult(requestCode, resultCode, data)
    }

    override fun onFbSignInFail() {
        Toast.makeText(this, "Facebook sign in failed.", Toast.LENGTH_SHORT).show()
    }

    override fun onFbSignInSuccess() {
        Toast.makeText(this, "Facebook sign in success", Toast.LENGTH_SHORT).show()
        startRegisterActivity()
    }

    override fun onFbProfileReceived(facebookUser: FacebookUser) {
        Toast.makeText(
            this,
            "Facebook user data: name= " + facebookUser.name + " email= " + facebookUser.email,
            Toast.LENGTH_SHORT
        ).show()

        Log.d("Person name: ", facebookUser.name + "")
        Log.d("Person gender: ", facebookUser.gender + "")
        Log.d("Person email: ", facebookUser.email + "")
        Log.d("Person image: ", facebookUser.facebookID + "")
    }

    override fun onFBSignOut() {
        Toast.makeText(this, "Facebook sign out success", Toast.LENGTH_SHORT).show()
    }

    override fun onLinkedInSignInFail() {
        Toast.makeText(this, "LinkedIn sign in failed.", Toast.LENGTH_SHORT).show()
    }

    override fun onLinkedInSignInSuccess(accessToken: String) {
        Toast.makeText(this, "Linked in signin successful.\n Getting user profile...", Toast.LENGTH_SHORT).show()
        startRegisterActivity()
    }

    override fun onLinkedInProfileReceived(user: LinkedInUser) {
        Toast.makeText(this, "LinkedIn user data: name= " + user.name + " email= " + user.email, Toast.LENGTH_SHORT)
            .show()
    }

    override fun onGoogleAuthSignIn(user: GoogleAuthUser) {
        Toast.makeText(this, "Google user data: name= " + user.name + " email= " + user.email, Toast.LENGTH_SHORT)
            .show()
        startRegisterActivity()
    }

    override fun onGoogleAuthSignInFailed() {
        Toast.makeText(this, "Google sign in failed.", Toast.LENGTH_SHORT).show()
    }

    override fun onGoogleAuthSignOut(isSuccess: Boolean) {
        Toast.makeText(this, if (isSuccess) "Sign out success" else "Sign out failed", Toast.LENGTH_SHORT).show()
    }

    private fun startRegisterActivity() {
        startActivity(RegisterActivity.startIntent(this))
        finish()
    }

    @SuppressLint("PackageManagerGetSignatures")
    @Suppress("DEPRECATION")
    fun getApplicationSignature(context: Context, packageName: String = context.packageName): List<String> {
        val signatureList: List<String>
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                // New signature
                val sig = context.packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNING_CERTIFICATES).signingInfo
                signatureList = if (sig.hasMultipleSigners()) {
                    // Send all with apkContentsSigners
                    sig.apkContentsSigners.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        bytesToHex(digest.digest())
                    }
                } else {
                    // Send one with signingCertificateHistory
                    sig.signingCertificateHistory.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        bytesToHex(digest.digest())
                    }
                }
            } else {
                val sig = context.packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures
                signatureList = sig.map {
                    val digest = MessageDigest.getInstance("SHA")
                    digest.update(it.toByteArray())
                    bytesToHex(digest.digest())
                }
            }

            return signatureList
        } catch (e: Exception) {
            // Handle error
        }
        return emptyList()
    }

    fun bytesToHex(bytes: ByteArray): String {
        val hexArray = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
        val hexChars = CharArray(bytes.size * 2)
        var v: Int
        for (j in bytes.indices) {
            v = bytes[j].toInt() and 0xFF
            hexChars[j * 2] = hexArray[v.ushr(4)]
            hexChars[j * 2 + 1] = hexArray[v and 0x0F]
        }
        return String(hexChars)
    }
}
