package com.allow.tinted.ui.authenticate.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.allow.tinted.domain.entities.data.Response
import com.allow.tinted.domain.entities.objects.Employee
import com.allow.tinted.domain.usecases.AddEmployeeUseCase
import java.lang.Exception

class RegisterViewModel(private val addEmployeeUseCase: AddEmployeeUseCase) : ViewModel() {

    private val _employee = MutableLiveData<Employee>()

    val registerLiveData: LiveData<Response<Unit, Exception>> = Transformations
        .switchMap(_employee) {
            if (it == null) {
                object : LiveData<Response<Unit, Exception>>() {
                    init {
                        postValue(null)
                    }
                }
            } else {
                object : LiveData<Response<Unit, Exception>>() {
                    init {
                        postValue(Response.loading())
                        // TODO replace with RxJava or Kotlin Coroutines
                        Thread {
                            try {
                                postValue(Response.success(addEmployeeUseCase.invoke(it)))
                            } catch (e: Exception) {
                                postValue(Response.error(e))
                            }
                        }.start()
                    }
                }
            }
        }

    fun registerUser(employee: Employee) {
        if (_employee.value != employee) {
            _employee.value = employee
        }
    }
}