package com.allow.tinted.ui.authenticate.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.allow.tinted.R
import com.allow.tinted.ui.authenticate.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    companion object {
        fun startIntent(ctx: Context) = Intent(ctx, LoginActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        sign_up_button.setOnClickListener {
            startActivity(RegisterActivity.startIntent(this))
            finish()
        }
    }
}
