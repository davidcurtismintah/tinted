package com.allow.tinted

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen

class TintEdApp : Application() {
    override fun onCreate() {
        super.onCreate()

        // initialise the JSR-310 time library
        initTimeLibrary()
    }

    private fun initTimeLibrary() {
        AndroidThreeTen.init(this)
    }
}
