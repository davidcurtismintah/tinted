package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.InstitutionsRepo
import com.allow.tinted.domain.entities.objects.Institution

class AddInstitutionUseCase(val repo: InstitutionsRepo) {
    operator fun invoke(institution: Institution) {
        repo.addInstitution(institution)
    }

}
