package com.allow.tinted.domain.entities.objects

import java.util.*

data class Skill(
    val id: String = UUID.randomUUID().toString(),
    val skill: String
)
