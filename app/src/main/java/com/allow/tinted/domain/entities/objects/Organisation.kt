package com.allow.tinted.domain.entities.objects

data class Organisation(
    val id: Long,
    val name: String,
    val contact: String,
    val email: String,
    val address: String
)