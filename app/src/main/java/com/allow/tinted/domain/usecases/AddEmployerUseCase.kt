package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.EmployersRepo
import com.allow.tinted.domain.entities.objects.Employer

class AddEmployerUseCase(val repo: EmployersRepo) {
    operator fun invoke(employer: Employer) {
        repo.addEmployer(employer)
    }

}
