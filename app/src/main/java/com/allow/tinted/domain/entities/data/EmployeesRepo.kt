package com.allow.tinted.domain.entities.data

import com.allow.tinted.domain.entities.objects.Employee

interface EmployeesRepo {
    fun addEmployee(employee: Employee)
    fun getEmployee(employeeId: String): Employee
}
