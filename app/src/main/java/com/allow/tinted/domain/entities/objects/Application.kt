package com.allow.tinted.domain.entities.objects

import org.threeten.bp.LocalDate
import java.math.BigDecimal

data class Application(
    val date: LocalDate,
    val description: String,
    val salary: BigDecimal,
    val jobId: Long
)
