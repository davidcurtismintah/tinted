package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.EmployeesRepo
import com.allow.tinted.domain.entities.objects.Employee

class GetEmployeeUseCase(private val repo: EmployeesRepo) {
    operator fun invoke(employeeId: String): Employee {
        return repo.getEmployee(employeeId)
    }
}
