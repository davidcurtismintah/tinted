package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.SkillsRepo
import com.allow.tinted.domain.entities.objects.Skill

/**
 * Issues a command to retrieve list of [skills][Skill] from the repository
 * */
class GetListOfSkillsUseCase(private val repo: SkillsRepo) {
    operator fun invoke(): List<Skill> = repo.retrieveAllSkills()
}