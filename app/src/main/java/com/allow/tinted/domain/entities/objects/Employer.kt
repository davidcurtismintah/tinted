package com.allow.tinted.domain.entities.objects

import org.threeten.bp.LocalDate
import java.util.*

data class Employer(
    val id: String = UUID.randomUUID().toString(),
    val name: String,
    val userName: String,
    val dateOfBirth: LocalDate,
    val contact: String,
    val email: String,
    val organisations: List<Organisation>,
    val jobs: List<Job>
): User