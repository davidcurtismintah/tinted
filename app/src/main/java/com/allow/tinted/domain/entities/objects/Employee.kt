package com.allow.tinted.domain.entities.objects

import org.threeten.bp.LocalDate
import java.util.*

data class Employee(
    val id: String = UUID.randomUUID().toString(),
    val name: String,
    val userName: String,
    val dateOfBirth: LocalDate,
    val contact: String,
    val email: String,
    /*TODO add user image*/
    val education: List<EmployeeEducation> = listOf(),
    val skills: List<EmployeeSkill> = listOf(),
    val experiences: List<Experience> = listOf(),
    val interests: List<Skill> = listOf(),
    val applications: List<Application> = listOf()
): User
