package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.QualificationsRepo
import com.allow.tinted.domain.entities.objects.Qualification
import com.allow.tinted.domain.entities.objects.Skill

/**
 * Issues a command to retrieve list of [qualifications][Qualification] from the repository
 * */
class GetListOfQualificationsUseCase(private val repo: QualificationsRepo) {
    operator fun invoke(): List<Qualification> = repo.retrieveAllQualifications()
}