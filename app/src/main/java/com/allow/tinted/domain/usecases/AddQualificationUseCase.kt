package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.QualificationsRepo
import com.allow.tinted.domain.entities.objects.Qualification

class AddQualificationUseCase(val repo: QualificationsRepo) {
    fun invoke(qualification: Qualification) {
        repo.addQualification(qualification)
    }

}
