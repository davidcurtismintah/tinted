package com.allow.tinted.domain.entities.objects

import java.util.*

data class Institution(
    val id: String = UUID.randomUUID().toString(),
    val name: String,
    val contact: String,
    val address: String
)