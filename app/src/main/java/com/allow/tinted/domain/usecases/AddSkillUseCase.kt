package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.SkillsRepo
import com.allow.tinted.domain.entities.objects.Skill

class AddSkillUseCase(val repo: SkillsRepo) {
    operator fun invoke(skill: Skill) {
        repo.addSkill(skill)
    }

}
