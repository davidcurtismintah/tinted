package com.allow.tinted.domain.entities.objects

import org.threeten.bp.LocalDate
import java.math.BigDecimal
import java.util.*

data class Job(
    val id: String = UUID.randomUUID().toString(),
    val userId: String,
    val title: String,
    val description: String,
    val type: JobType,
    val startDate: LocalDate,
    val endDate: LocalDate,
    val salary: BigDecimal,
    val location: String,
    val requiredSkills: List<Skill>
)