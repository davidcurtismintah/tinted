package com.allow.tinted.domain.entities.data

import com.allow.tinted.domain.entities.objects.Employer

interface EmployersRepo {
    fun addEmployer(employer: Employer)
}
