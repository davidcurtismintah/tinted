package com.allow.tinted.domain.entities.objects

import org.threeten.bp.LocalDate

data class Experience(
    val id: Long,
    val isEmployed: Boolean,
    val description: String,
    val type: JobType,
    val startDate: LocalDate,
    val endDate: LocalDate
)
