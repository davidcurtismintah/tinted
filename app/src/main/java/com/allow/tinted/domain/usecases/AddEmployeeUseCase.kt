package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.EmployeesRepo
import com.allow.tinted.domain.entities.objects.Employee

class AddEmployeeUseCase(private val repo: EmployeesRepo) {
    operator fun invoke(employee: Employee) {
        return repo.addEmployee(employee)
    }
}
