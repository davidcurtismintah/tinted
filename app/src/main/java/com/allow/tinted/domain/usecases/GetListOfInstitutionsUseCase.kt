package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.InstitutionsRepo
import com.allow.tinted.domain.entities.objects.Institution

/**
 * Issues a command to retrieve list of [institutions][Institution] from the repository
 * */
class GetListOfInstitutionsUseCase(private val repo: InstitutionsRepo) {
    operator fun invoke(): List<Institution> = repo.retrieveAllInstitutions()
}