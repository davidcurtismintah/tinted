package com.allow.tinted.domain.entities.objects

data class EmployeeEducation(
    val aggregate: String,
    val institution: Institution,
    val qualification: Qualification
)
