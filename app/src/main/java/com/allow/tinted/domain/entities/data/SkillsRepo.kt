package com.allow.tinted.domain.entities.data

import com.allow.tinted.domain.entities.objects.Skill

interface SkillsRepo {
    fun retrieveAllSkills(): List<Skill>
    fun addSkill(skill: Skill)
}
