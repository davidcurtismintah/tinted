package com.allow.tinted.domain.entities.data

import com.allow.tinted.domain.entities.objects.Qualification

interface QualificationsRepo {
    fun retrieveAllQualifications(): List<Qualification>
    fun addQualification(qualification: Qualification)
}
