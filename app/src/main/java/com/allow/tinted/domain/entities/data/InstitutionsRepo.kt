package com.allow.tinted.domain.entities.data

import com.allow.tinted.domain.entities.objects.Institution

interface InstitutionsRepo {
    fun retrieveAllInstitutions(): List<Institution>
    fun addInstitution(institution: Institution)
}
