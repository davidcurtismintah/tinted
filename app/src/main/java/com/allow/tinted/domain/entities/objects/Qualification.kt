package com.allow.tinted.domain.entities.objects

import org.threeten.bp.Period
import java.util.*

data class Qualification(
    val id: String = UUID.randomUUID().toString(),
    val degree: String,
    val domain: String,
    val duration: Period
)