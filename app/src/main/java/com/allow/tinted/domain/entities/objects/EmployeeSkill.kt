package com.allow.tinted.domain.entities.objects

import org.threeten.bp.Period

data class EmployeeSkill(
    val skill: Skill,
    val duration: Period
)
