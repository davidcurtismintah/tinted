package com.allow.tinted.domain.entities.objects

enum class JobType {
    INTERN, FULL_TIME
}
