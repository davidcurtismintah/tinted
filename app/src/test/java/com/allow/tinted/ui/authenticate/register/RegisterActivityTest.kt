package com.allow.tinted.ui.authenticate.register

import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RegisterActivityTest {

    @Test
    fun testEvent() {
        val scenario = ActivityScenario.launch(RegisterActivity::class.java)
        scenario.onActivity { activity ->
            assertThat(activity.packageName).isEqualTo("com.allow.tinted")
        }


    }
}