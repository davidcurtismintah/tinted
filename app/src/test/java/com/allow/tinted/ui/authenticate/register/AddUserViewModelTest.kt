package com.allow.tinted.ui.authenticate.register

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.allow.tinted.domain.entities.data.Response
import com.allow.tinted.domain.entities.data.Status
import com.allow.tinted.domain.entities.objects.Employee
import com.allow.tinted.domain.usecases.AddEmployeeUseCase
import com.google.common.truth.Truth.assertThat
import io.mockk.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.util.concurrent.CountDownLatch

@RunWith(JUnit4::class)
class AddUserViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun registersUserProperly() {
        val employee = mockk<Employee>()

        val addEmployeeUseCase = mockk<AddEmployeeUseCase>()
        every { addEmployeeUseCase.invoke(employee) } just Runs

        val viewModel = RegisterViewModel(addEmployeeUseCase)

        val latch = CountDownLatch(2)
        val responses = mutableListOf<Response<Unit, Exception>>()
        val observer = mockk<Observer<Response<Unit, Exception>>>()
        every { observer.onChanged(capture(responses)) } answers {
            latch.countDown()
            if (latch.count == 0L){
                viewModel.registerLiveData.removeObserver(observer)
            }
        }

        viewModel.registerLiveData.observeForever(observer)
        viewModel.registerUser(employee)

        assertThat(responses).hasSize(2)
        assertThat(responses[0].status).isEqualTo(Status.LOADING)
        assertThat(responses[1].status).isAnyOf(Status.SUCCESS, Status.ERROR)

        verify { addEmployeeUseCase.invoke(employee) }

        confirmVerified(addEmployeeUseCase)
    }
}
