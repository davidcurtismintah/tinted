package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.InstitutionsRepo
import com.allow.tinted.domain.entities.objects.Institution
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

class AddInstitutionTest {

    @MockK
    lateinit var institutionsRepo: InstitutionsRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `adds new institution to repository correctly`() {
        val useCase = AddInstitutionUseCase(institutionsRepo)

        val institution = mockk<Institution> {
            io.mockk.every { name } returns "Example University1"
            io.mockk.every { address } returns "Example Address1"
            io.mockk.every { contact } returns "+233000000001"
        }

        every { institutionsRepo.addInstitution(institution) } just Runs

        useCase.invoke(institution)

        verify { institutionsRepo.addInstitution(institution) }

        confirmVerified(institutionsRepo)
    }
}

