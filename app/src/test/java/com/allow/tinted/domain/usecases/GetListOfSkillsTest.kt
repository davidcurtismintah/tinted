package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.SkillsRepo
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test

class GetListOfSkillsTest {

    @MockK
    lateinit var skillsRepo: SkillsRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every { skillsRepo.retrieveAllSkills() } returns listOf(
            mockk {
                every { skill } returns "Android Development"
            },
            mockk {
                every { skill } returns "Database Design"
            },
            mockk {
                every { skill } returns "Agile Methodology"
            }
        )
    }

    @Test
    fun `gets list of skills from repository successfully`() {
        val useCase = GetListOfSkillsUseCase(skillsRepo)

        val institutionsList = useCase.invoke()

        verify { skillsRepo.retrieveAllSkills() }

        assertThat(institutionsList.size, `is`(3))

        confirmVerified(skillsRepo)
    }
}
