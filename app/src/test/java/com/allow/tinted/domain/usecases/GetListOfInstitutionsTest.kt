package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.InstitutionsRepo
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test

class GetListOfInstitutionsTest {

    @MockK
    lateinit var institutionsRepo: InstitutionsRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every { institutionsRepo.retrieveAllInstitutions() } returns listOf(
            mockk {
                every { name } returns "Example University1"
                every { address } returns "Example Address1"
                every { contact } returns "+233000000001"
            },
            mockk {
                every { name } returns "Example University2"
                every { address } returns "Example Address2"
                every { contact } returns "+233000000002"
            },
            mockk {
                every { name } returns "Example University3"
                every { address } returns "Example Address3"
                every { contact } returns "+233000000003"
            }
        )
    }

    @Test
    fun `gets list of institutions from repository successfully`() {
        val useCase = GetListOfInstitutionsUseCase(institutionsRepo)

        val institutionsList = useCase.invoke()

        verify { institutionsRepo.retrieveAllInstitutions() }

        assertThat(institutionsList.size, `is`(3))

        confirmVerified(institutionsRepo)
    }
}
