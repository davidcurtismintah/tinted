package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.SkillsRepo
import com.allow.tinted.domain.entities.objects.Skill
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

class AddSkillTest {

    @MockK
    lateinit var skillsRepo: SkillsRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `adds new skill to repository correctly`() {
        val useCase = AddSkillUseCase(skillsRepo)

        val skill = mockk<Skill> {
            every { skill } returns "Android Developer"
        }

        every { skillsRepo.addSkill(skill) } just Runs

        useCase.invoke(skill)

        verify { skillsRepo.addSkill(skill) }

        confirmVerified(skillsRepo)
    }
}

