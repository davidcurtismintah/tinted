package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.EmployeesRepo
import com.allow.tinted.domain.entities.objects.Employee
import com.allow.tinted.domain.entities.objects.JobType
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Period
import java.math.BigDecimal

class AddEmployeeTest {

    companion object {
        const val SAMPLE_PHONE_NUMBER = "+233000000000"
        const val SAMPLE_EMAIL = "john.doe@example.com"

        val SAMPLE_DATE_OF_BIRTH = LocalDate.of(2000, 1, 1)

        const val SAMPLE_AGGREGATE_MADE = "4.0"

        val SAMPLE_START_EDUCATION_DATE = LocalDate.of(2000, 10, 3)
        val SAMPLE_END_EDUCATION_DATE = LocalDate.of(2019, 10, 3)

        val SAMPLE_DATE_OF_APPLICATION = LocalDate.of(2019, 12, 1)
    }

    @MockK
    lateinit var employeesRepo: EmployeesRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `adds new employee to repository correctly`() {
        val useCase = AddEmployeeUseCase(employeesRepo)

        val employee = mockk<Employee> {
            every { name } returns "John Doe"
            every { userName } returns "john-doe"
            every { dateOfBirth } returns SAMPLE_DATE_OF_BIRTH
            every { contact } returns SAMPLE_PHONE_NUMBER
            every { email } returns SAMPLE_EMAIL
            every { education } returns listOf(
                mockk {
                    every { aggregate } returns SAMPLE_AGGREGATE_MADE
                    every { institution } returns mockk {
                        every { name } returns "Example University1"
                        every { address } returns "Example Address1"
                        every { contact } returns SAMPLE_PHONE_NUMBER
                    }
                    every { qualification } returns mockk {
                        every { degree } returns "BSc"
                        every { domain } returns "Computer Science"
                        every { duration } returns Period.ofYears(4)
                    }
                }
            )
            every { skills } returns listOf(
                mockk {
                    every { skill } returns mockk {
                        every { skill } returns "Android Developer"
                    }
                    every { duration } returns Period.ofYears(6)
                }
            )
            every { experiences } returns listOf(
                mockk {
                    every { isEmployed } returns false
                    every { description } returns "Sample Work Experience Description"
                    every { type } returns JobType.INTERN
                    every { startDate } returns SAMPLE_START_EDUCATION_DATE
                    every { endDate } returns SAMPLE_END_EDUCATION_DATE
                }
            )
            every { interests } returns listOf(
                mockk {
                    every { skill } returns "Android Developer"
                }
            )
            every { applications } returns listOf(
                mockk {
                    every { date } returns SAMPLE_DATE_OF_APPLICATION
                    every { description } returns "Sample Job Description"
                    every { salary } returns BigDecimal.ZERO
                }
            )
        }

        every { employeesRepo.addEmployee(employee) } just Runs

        useCase.invoke(employee)

        verify { employeesRepo.addEmployee(employee) }

        confirmVerified(employeesRepo)
    }
}

