package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.EmployeesRepo
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.hamcrest.core.Is.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.threeten.bp.Period

class GetEmployeeTest {

    companion object {
        const val SAMPLE_EMPLOYEE_ID = "123456789"
    }

    @MockK
    lateinit var employeesRepo: EmployeesRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `gets employee by ID from repository correctly`() {
        val useCase = GetEmployeeUseCase(employeesRepo)

        every { employeesRepo.getEmployee(SAMPLE_EMPLOYEE_ID) } returns mockk {
            every { id } returns SAMPLE_EMPLOYEE_ID
            every { name } returns "John Doe"
            every { userName } returns "john-doe"
            every { dateOfBirth } returns AddEmployeeTest.SAMPLE_DATE_OF_BIRTH
            every { contact } returns "+233000000000"
            every { email } returns "john.doe@example.com"
            every { education } returns listOf(
                mockk {
                    every { aggregate } returns AddEmployeeTest.SAMPLE_AGGREGATE_MADE
                    every { institution } returns mockk {
                        every { name } returns "Example University1"
                        every { address } returns "Example Address1"
                        every { contact } returns "+233000000001"
                    }
                    every { qualification } returns mockk {
                        every { degree } returns "BSc"
                        every { domain } returns "Computer Science"
                        every { duration } returns Period.ofYears(4)
                    }
                }
            )
        }

        val employee = useCase.invoke(SAMPLE_EMPLOYEE_ID)

        assertThat(employee.id, `is`(SAMPLE_EMPLOYEE_ID))

        verify { employeesRepo.getEmployee(SAMPLE_EMPLOYEE_ID) }

        confirmVerified(employeesRepo)
    }
}