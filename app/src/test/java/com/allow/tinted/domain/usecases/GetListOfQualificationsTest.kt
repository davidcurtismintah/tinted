package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.QualificationsRepo
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.threeten.bp.Period

class GetListOfQualificationsTest {

    @MockK
    lateinit var qualificationsRepo: QualificationsRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every { qualificationsRepo.retrieveAllQualifications() } returns listOf(
            mockk {
                every { degree } returns "BSc"
                every { domain } returns "Computer Science"
                every { duration } returns Period.ofYears(4)
            },
            mockk {
                every { degree } returns "BA"
                every { domain } returns "Information Studies"
                every { duration }returns Period.ofYears(4)
            },
            mockk {
                every { degree } returns "BSc"
                every { domain } returns "Engineering"
                every { duration } returns Period.ofYears(4)
            }
        )
    }

    @Test
    fun `gets list of qualifications from repository successfully`() {
        val useCase = GetListOfQualificationsUseCase(qualificationsRepo)

        val institutionsList = useCase.invoke()

        verify { qualificationsRepo.retrieveAllQualifications() }

        assertThat(institutionsList.size, `is`(3))

        confirmVerified(qualificationsRepo)
    }
}
