package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.QualificationsRepo
import com.allow.tinted.domain.entities.objects.Qualification
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test
import org.threeten.bp.Period

class AddQualificationTest {

    @MockK
    lateinit var qualificationsRepo: QualificationsRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `adds new qualification to repository correctly`() {
        val useCase = AddQualificationUseCase(qualificationsRepo)

        val qualification = mockk<Qualification> {
            every { degree } returns "BSc"
            every { domain } returns "Computer Science"
            every { duration } returns Period.ofYears(4)
        }

        every { qualificationsRepo.addQualification(qualification) } just Runs

        useCase.invoke(qualification)

        verify { qualificationsRepo.addQualification(qualification) }

        confirmVerified(qualificationsRepo)
    }
}

