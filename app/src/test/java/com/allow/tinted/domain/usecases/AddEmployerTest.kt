package com.allow.tinted.domain.usecases

import com.allow.tinted.domain.entities.data.EmployersRepo
import com.allow.tinted.domain.entities.objects.Employer
import com.allow.tinted.domain.entities.objects.JobType
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test
import org.threeten.bp.LocalDate
import java.math.BigDecimal

class AddEmployerTest {

    companion object {
        const val SAMPLE_PHONE_NUMBER = "+233000000000"
        const val SAMPLE_EMAIL = "john.doe@example.com"

        val SAMPLE_DATE_OF_BIRTH = LocalDate.of(2000, 1, 1)

        val SAMPLE_START_JOB_DATE = LocalDate.of(2000, 10, 3)
        val SAMPLE_END_JOB_DATE = LocalDate.of(2019, 10, 3)

        val SAMPLE_DATE_OF_APPLICATION = LocalDate.of(2019, 12, 1)
    }

    @MockK
    lateinit var employersRepo: EmployersRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `adds new employer to repository correctly`() {
        val useCase = AddEmployerUseCase(employersRepo)

        val employee = mockk<Employer> {
            every { name } returns "John Doe"
            every { userName } returns "john.doe"
            every { dateOfBirth } returns SAMPLE_DATE_OF_BIRTH
            every { contact } returns SAMPLE_PHONE_NUMBER
            every { email } returns SAMPLE_EMAIL
            every { organisations } returns listOf(
                mockk {
                    every { name } returns "Sample Organisation"
                    every { address } returns "Sample Organisation Address"
                    every { contact } returns SAMPLE_PHONE_NUMBER
                    every { email } returns "sample.organisation@example.com"
                }
            )
            every { jobs } returns listOf(
                mockk {
                    every { userId } returns "Sample User ID"
                    every { title } returns "Sample Job Title"
                    every { description } returns "Sample Job Description"
                    every { type } returns JobType.INTERN
                    every { startDate } returns SAMPLE_START_JOB_DATE
                    every { endDate } returns SAMPLE_END_JOB_DATE
                    every { salary } returns BigDecimal("10000")
                    every { location } returns "Sample Job Location"
                    every { requiredSkills } returns listOf(
                        mockk {
                            every { skill } returns "Android Developer"
                        }
                    )
                }
            )
        }

        every { employersRepo.addEmployer(employee) } just Runs

        useCase.invoke(employee)

        verify { employersRepo.addEmployer(employee) }

        confirmVerified(employersRepo)
    }
}

